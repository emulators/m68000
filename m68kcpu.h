/* ======================================================================== */
/* ========================= LICENSING & COPYRIGHT ======================== */
/* ======================================================================== */
/*

 */

#ifndef M68KCPU__HEADER
#define M68KCPU__HEADER

#define INSTRSIZE	50


typedef enum
{
	CPU_STATUS_NULL=0,
	CPU_STATUS_RESET,
	CPU_STATUS_BUS_ERROR,
	CPU_STATUS_ADDRESS_ERROR,
	CPU_STATUS_ILLEGAL_INST,
	CPU_STATUS_DIVID_ZERO,
	CPU_STATUS_CHK_INST,
	CPU_STATUS_TRAPV_INST,
	CPU_STATUS_PRIVILEGE_VIOLATION,
	CPU_STATUS_TRACE,
}CPU_STATUS;


typedef struct _cpu_contex
{
	LIST_ITEM *memory;				//memory is a list of stuctures of memory data
	CPU_STATUS status;
	unsigned short sr;
	unsigned int cycles;
	unsigned int d[8];
	unsigned int a[8];
	unsigned int ssp;
	unsigned char isize;			//size mode
	unsigned int pc;				//program counter
	unsigned short ir;				//current instruction register
	char *istr;						//current instruction string
	unsigned long address;			//address to generate intructuion string
	unsigned short code;			//code located a address
}CPU_CONTEX;

extern unsigned long M68K_Address_Reg_Read(CPU_CONTEX *cpu,unsigned char reg);
extern bool M68K_Address_Reg_Write(CPU_CONTEX *cpu,unsigned char reg,unsigned long data);
extern bool M68K_Close(CPU_CONTEX *cpu);
extern bool M68K_Memory_Add(CPU_CONTEX *cpu, MEMORY_STRUCT *ms);
extern int M68K_Execute(CPU_CONTEX *cpu,int num_cycles);
extern bool M68K_Init(CPU_CONTEX *cpu);
extern bool M68K_Pulse_Reset(CPU_CONTEX *cpu);
extern bool M68K_PC_Read(CPU_CONTEX *cpu,unsigned long *data);
extern bool M68k_PC_Write(CPU_CONTEX *cpu,unsigned long data);
extern bool M68K_Read_Long(CPU_CONTEX *cpu,unsigned long address,unsigned long *data);
extern bool M68K_Read_Byte(CPU_CONTEX *cpu,unsigned int address,unsigned char *data);
extern bool M68K_Read_Word(CPU_CONTEX *cpu,unsigned int address,unsigned short *data);
extern int M68K_Memory_Reset(CPU_CONTEX *cpu);
extern bool M68K_Super_Read(CPU_CONTEX *cpu);
extern bool M68K_Write_Long(CPU_CONTEX *cpu,unsigned int address,unsigned long data);
extern bool M68K_Write_Byte(CPU_CONTEX *cpu,unsigned int address,unsigned char data);
extern bool M68K_Write_Word(CPU_CONTEX *cpu,unsigned int address,unsigned short data);
extern bool M68K_Neg_Read(CPU_CONTEX *cpu);
extern void M68K_Neg_Write(CPU_CONTEX *cpu,bool bit);
extern void M68K_Zero_Write(CPU_CONTEX *cpu,bool bit);
extern bool M68K_Zero_Read(CPU_CONTEX *cpu);
extern bool M68K_VFlag_Read(CPU_CONTEX *cpu);
extern void M68K_VFlag_Write(CPU_CONTEX *cpu,bool bit);
extern bool M68K_Carry_Read(CPU_CONTEX *cpu);
extern void M68K_Carry_Write(CPU_CONTEX *cpu,bool bit);
#endif /* M68KCPU__HEADER */
