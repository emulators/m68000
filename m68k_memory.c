#include <stdio.h>
#include <stdlib.h>
//#include <stdarg.h>
#include <stdbool.h>

#include "list.h"
#include "m68k_memory.h"

typedef union
{
	unsigned long lg;
	unsigned short lgs[2];
	unsigned char lgc[4];
}LONG_TYPE;





//******************************************************************************
//Description:	Read memory long
//Arguments:	Pointer to memory structure
//				Address
//				pointer long return data
//Return:		True
//				False
//******************************************************************************
bool Memory_Read_Long(MEMORY_STRUCT *ms,unsigned int address,unsigned long *data)
{
	bool retval = false;
	LONG_TYPE d;
	unsigned char *mptr = NULL;

	d.lg = 0;
	if(ms != NULL)
	{
		if((ms->start != ms->end) && (ms->mptr != NULL))
		{
			if((address >= ms->start) && (address <= ms->end))
			{
				mptr = ms->mptr + (address - ms->start);
				d.lgc[3] = *mptr++;
				d.lgc[2] = *mptr++;
				d.lgc[1] = *mptr++;
				d.lgc[0] = *mptr++;
				*data = d.lg;
				if(ms->read != NULL)
				{
					(ms->read)(ms,&d.lg);
				}
				retval = true;
			}
		}
	}

	return(retval);
}

//******************************************************************************
//Description:	Read memory long
//Arguments:	Pointer to memory structure
//				Address
//				pointer long return data
//Return:		True
//				False
//******************************************************************************
bool Memory_Write_Long(MEMORY_STRUCT *ms,unsigned int address,unsigned long data)
{
	bool retval = false;
	LONG_TYPE d;
	unsigned char *mptr = NULL;

	d.lg = data;
	if(ms != NULL)
	{
		if((ms->start != ms->end) && (ms->mptr != NULL))
		{
			if((address >= ms->start) && (address <= ms->end))
			{
				mptr = ms->mptr + (address - ms->start);
				*mptr++ = d.lgc[3];
				*mptr++ = d.lgc[2];
				*mptr++ = d.lgc[1];
				*mptr = d.lgc[0];
				if(ms->write != NULL)
				{
					(ms->write)(ms,&d.lg);
				}
				retval = true;
			}
		}
	}

	return(retval);
}

//**********************************************************************
//Description:	Read word
//Arguments:	Pointer to Memory structure
//				address
//				pointer to word data
//Return:		true
//				false
//**********************************************************************
bool Memory_Read_Word(MEMORY_STRUCT *ms, unsigned int address, unsigned short *data)
{
	bool retval = false;
	LONG_TYPE d;
	unsigned char *mptr;

	d.lg = 0;
	if(ms != NULL)
	{
		if((ms->start != ms->end) && (ms->mptr != NULL))
		{
			if((address >= ms->start) && (address <= ms->end))
			{
				mptr = ms->mptr + (address - ms->start);
				d.lgc[1] = *mptr++;
				d.lgc[0] = *mptr++;
				*data = d.lgs[0];
				retval = true;
			}
		}
	}
	return(retval);
}


//**********************************************************************
//Description:	Read word
//Arguments:	Pointer to Memory structure
//				address
//				pointer to word data
//Return:		true
//				false
//**********************************************************************
bool Memory_Write_Word(MEMORY_STRUCT *ms, unsigned int address, unsigned short data)
{
	bool retval = false;
	LONG_TYPE d;
	unsigned char *mptr;

	d.lg = data;
	if(ms != NULL)
	{
		if((ms->start != ms->end) && (ms->mptr != NULL))
		{
			if((address >= ms->start) && (address <= ms->end))
			{
				mptr = ms->mptr + (address - ms->start);
				*mptr++ = d.lgc[1];
				*mptr = d.lgc[0];
				retval = true;
			}
		}
	}
	return(retval);
}

//**********************************************************************
//Description:	Read byte
//Arguments:	Pointer to Memory structure
//				address
//				pointer to word data
//Return:		true
//				false
//**********************************************************************
bool Memory_Read_Byte(MEMORY_STRUCT *ms, unsigned int address, unsigned char *data)
{
	bool retval = false;
	unsigned char *mptr;

	if(ms != NULL)
	{
		if((ms->start != ms->end) && (ms->mptr != NULL))
		{
			if((address >= ms->start) && (address <= ms->end))
			{
				mptr = ms->mptr + (address - ms->start);
				*data = *mptr;
				retval = true;
			}
		}
	}
	return(retval);
}

//**********************************************************************
//Description:	Read byte
//Arguments:	Pointer to Memory structure
//				address
//				pointer to word data
//Return:		true
//				false
//**********************************************************************
bool Memory_Write_Byte(MEMORY_STRUCT *ms, unsigned int address, unsigned char data)
{
	bool retval = false;
	unsigned char *mptr;

	if(ms != NULL)
	{
		if((ms->start != ms->end) && (ms->mptr != NULL))
		{
			if((address >= ms->start) && (address <= ms->end))
			{
				mptr = ms->mptr + (address - ms->start);
				*mptr = data;
				retval = true;
			}
		}
	}
	return(retval);
}



//***********************************************************
//Description:	Allocate new memory using start and end
//Argument:		Pointer to memory
//Return:		True
//				false
//*************************************************************
bool Memory_Allocate(MEMORY_STRUCT *ms)
{
	bool retval = false;
	int sz;

	if(ms != NULL)
	{
		sz = Memory_Size(ms);
		if(sz != 0)
		{
			if(ms->mptr)
			{
				//pointer has been set allready
				free(ms->mptr);
				ms->mptr = NULL;
			}

			ms->mptr = malloc(sz);
			if(ms->mptr)
			{
				retval = true;
			}
		}
	}
	return(retval);
}

//*************************************************************************
//Description:	Load file into ROM memory
//Arguments:	Pointer to CPU Structure
//				Pointer to file name string
//Retrun:		True for success
//				False for bad
//**************************************************************************
bool Memory_FileLoad(MEMORY_STRUCT *ms,char *fn)
{
	FILE *fh;
	bool retval = false;
	int sz;


	if((ms != NULL) && (fn != NULL))
	{
		sz = ms->end - ms->start;
		if(sz != 0)
		{
			if(ms->mptr != NULL)
			{
				if((fh = fopen(fn, "rb")) != NULL)
				{
					if(fread(ms->mptr, 1, sz, fh) > 0)
					{
						fclose(fh);
						retval = true;
					}
				}
			}
		}
	}
	return(retval);
}



//***********************************************************
//Description:	Return pointer to memory location
//Arguements:	pointer to Memory structure
//Returns:		Pointer to memory
//************************************************************
unsigned char *Memory_Pointer(MEMORY_STRUCT *ms)
{
	unsigned char *retval = NULL;

	if(ms != NULL)
	{
		retval = ms->mptr;
	}
	return(retval);
}

//************************************************************
//Description:	Return the size of memory
//Arguments:	Pointer to memory pointer
//Return:		Size
//************************************************************
int Memory_Size(MEMORY_STRUCT *ms)
{
	int retval = 0;

	if(ms != NULL)
	{
		retval = (ms->end - ms->start);
	}
	return(retval);
}


//*********************************************************
//Description:	Set start and end address of memory
//Arguments:	Pointer to memory structure
//				start
//				end
//Reurn:		true
//				false
//********************************************************
bool Memory_Start_End(MEMORY_STRUCT *ms,int start,int end)
{
	bool retval = false;

	if((ms != NULL) && (start != end))
	{
		ms->start = start;
		ms->end = end;
		if(Memory_Allocate(ms))
		retval = true;
	}
	return(retval);
}

//*********************************************************
//Description: Set start of memory and size of memory
//Arguments:	pointer to memory structure
//				start
//				size
//Return:		true
//				false
//*********************************************************
bool Memory_Start_Size(MEMORY_STRUCT *ms,int start,int size)
{
	bool retval = false;

	if((ms != NULL) && (size != 0))
	{
		ms->start = start;
		ms->end = start + size;
		if(Memory_Allocate(ms))
		{
			retval = true;
		}
	}
	return(retval);
}



//*********************************************************
//Description:	Create a memory structure and add
//Retruns:		Pointer to Memory structure
//********************************************************
MEMORY_STRUCT *Memory_Create(unsigned int start,unsigned int size)
{
	MEMORY_STRUCT *retval = NULL;

	retval = malloc(sizeof(MEMORY_STRUCT));
	if(retval != NULL)
	{
		retval->read = NULL;
		retval->write = NULL;
		retval->reset = NULL;
		retval->start = start;
		retval->end = start + size;
		retval->mptr = malloc(size);
		if(retval->mptr != NULL)
		{
			retval->mlist.next = &retval->mlist;
			retval->mlist.previous = &retval->mlist;
			retval->mlist.owner = retval;
		}
	}
	return(retval);
}


//******************************************************************************
//Description:	Destroy the memory sructure
//Arguments:	Pointer to Memory_Allocate(
//Return:		Number memory structure were destroyed
//*****************************************************************************
bool Memory_Close(MEMORY_STRUCT *ms)
{
	bool retval = 0;

	if(ms != NULL)
	{
		if(ms->mptr != NULL)
		{
			free(ms->mptr);
			ms->mptr = NULL;
			retval = true;
		}
		free(ms);
		ms = NULL;
	}
	return(retval);
}

