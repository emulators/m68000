/*
bit functions
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>



bool bit_test_byte(unsigned char b, int bit)
{
    unsigned char mask = 1;
    bool retval = false;

    mask <<= bit;
    if(b && mask) retval = true;

    return(retval);

}


bool bit_test_word(unsigned short b, int bit)
{
    unsigned short mask = 1;
    bool retval = false;

    mask <<= bit;
    if(b && mask) retval = true;
    return(retval);
}

bool bit_test_long(unsigned long b, int bit)
{
    unsigned long mask = 1;
    bool retval = false;

    mask <<= bit;
    if(b && mask) retval = true;
    return(retval);
}

