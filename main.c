#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#include "list.h"
#include "m68k_memory.h"
#include "m68kcpu.h"
#include "m68k_opstr.h"

/* Memory-mapped IO ports */
#define INPUT_ADDRESS 0x800000
#define OUTPUT_ADDRESS 0x400000

/* IRQ connections */
#define IRQ_NMI_DEVICE 7
#define IRQ_INPUT_DEVICE 2
#define IRQ_OUTPUT_DEVICE 1

/* ROM and RAM sizes */
#define MAX_ROM 0xfff
#define MAX_RAM 0xff

#define CR	0x0d
#define ESC 0x1b

unsigned char g_ram[MAX_RAM+1];					/* RAM */
unsigned int g_fc;								/* Current function code from CPU */


/* Exit with an error message.  Use printf syntax. */
void exit_error(char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fprintf(stderr, "\n");

	exit(EXIT_FAILURE);
}

//*******************************************************
//Description:	Fix up the memory structure
//Arguments:	pointer to Memory Struct
//*********************************************************
void Fix_Memory(MEMORY_STRUCT *mr,void *data)
{
	unsigned int size;

	if(mr != NULL)
	{
		size = mr->end - mr->start;

		mr->start = 0x500000;
		mr->end = mr->start + size;
		mr->write = NULL;
	}
}

int main(int argc, char* argv[])
{
	CPU_CONTEX cpu;		//make cpu
	MEMORY_STRUCT *ms = NULL;
	bool end = false;


	if(argc == 2)
	{
		char kb;
		M68K_Init(&cpu);
		if((ms = Memory_Create(0,1000)) != NULL) ms->write = Fix_Memory;
		Memory_FileLoad(ms,argv[1]);
		M68K_Memory_Add(&cpu,ms);
		ms = Memory_Create(0x500000,1000);
		Memory_FileLoad(ms,argv[1]);
		M68K_Memory_Add(&cpu,ms);
		M68K_Pulse_Reset(&cpu);
		while(!end)
		{
			M68K_PC_Read(&cpu,&cpu.address);
			OPStr_Decode(&cpu);
			printf("%s",cpu.istr);

			kb = getchar();
			if(kb == ESC)
			{
				end = true;
			}
			else if(kb == 's')
			{
				M68K_Execute(&cpu,1);
			}
		}

	}
	else
	{
		exit_error("Usage: sim <program file>");
	}

	M68K_Close(&cpu);	//frees up any memory that is allocated
    return 0;
}
