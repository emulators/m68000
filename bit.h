#ifndef BIT_H
#define BIT_H

extern bool bit_test_byte(unsigned char b, int bit);
extern bool bit_test_word(unsigned short b, int bit);
extern bool bit_test_long(unsigned long b, int bit);

#endif // BIT_H
