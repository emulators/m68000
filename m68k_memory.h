
#ifndef M68K_MEMORY_H
#define M68K_MEMORY_H


typedef struct _cpu_memory
{
	LIST_ITEM mlist;	//this structure can be part of list
	int start;		//start location
	int end;		//end location
	unsigned char *mptr;	//byte pointer to location
	void (*write)(struct _cpu_memory *,void *);
	void (*read)(struct _cpu_memory *,void *);
	void (*reset)(void *);
}MEMORY_STRUCT;


extern MEMORY_STRUCT *Memory_Create(unsigned int start,unsigned int size);
extern bool Memory_Close(MEMORY_STRUCT *ms);
extern bool Memory_FileLoad(MEMORY_STRUCT *ms,char *fn);
extern unsigned char *Memory_Pointer(MEMORY_STRUCT *ms);
extern bool Memory_Read_Long(MEMORY_STRUCT *ms, unsigned int address, unsigned long *data);
extern bool Memory_Read_Word(MEMORY_STRUCT *ms, unsigned int address, unsigned short *data);
extern bool Memory_Read_Byte(MEMORY_STRUCT *ms, unsigned int address, unsigned char *data);
extern bool Memory_Write_Long(MEMORY_STRUCT *ms, unsigned int address, unsigned long data);
extern bool Memory_Write_Word(MEMORY_STRUCT *ms, unsigned int address, unsigned short data);
extern bool Memory_Write_Byte(MEMORY_STRUCT *ms, unsigned int address, unsigned char data);
extern int Memory_Size(MEMORY_STRUCT *ms);
extern bool Memory_Start_End(MEMORY_STRUCT *ms,int start,int end);
extern bool Memory_Start_Size(MEMORY_STRUCT *ms,int start,int size);

#endif
