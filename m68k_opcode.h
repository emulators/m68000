#ifndef M68K_OPCODE_H
#define M68K_OPCODE_H

typedef struct
{
	unsigned scr_reg:3;
	unsigned scr_mode:3;
	unsigned dst_mode:3;
	unsigned dst_reg:3;
	unsigned size:2;
	unsigned xx:2;
}MOVE_STRUCT;

extern void OPCode_Decode(CPU_CONTEX *cpu);

#endif
