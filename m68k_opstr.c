//**************************************************

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>


#include "list.h"
#include "m68k_memory.h"
#include "m68kcpu.h"
#include "m68k_opcode.h"

/* This is used to generate the opcode handler jump table */
typedef struct
{
	unsigned int  mask;                  /* mask on opcode */
	unsigned int  match;                 /* what to match after masking */
	void (*opcode_handler)(void *);        /* handler function */
	//unsigned char cycles[NUM_CPU_TYPES]; /* cycles each cpu type takes */
}OPSTR_STRUCT;



//*******************************************************
//Decription:	Mode Register encode
//Argument:		Pointer CPU contex
//				Mode
//				Reg
//Return:		Data
//*******************************************************
void OPStr_Mode_Seven(CPU_CONTEX *cpu,unsigned char mode,unsigned char reg)
{
	unsigned long a;

	if(cpu != NULL)
	{

		if(mode == 7)
		{
			switch(reg)
			{
				case 0:	//short
					M68K_Read_Word(cpu,cpu->address+2,(unsigned short *)&a);
					sprintf(cpu->istr,"%s%4.4X",cpu->istr,(unsigned short) a);
					break;

				case 1: //long
					M68K_Read_Long(cpu,cpu->address+2,&a);
					sprintf(cpu->istr,"%s%8.8X",cpu->istr,(unsigned int)a);
					break;

				case 4: //SR
					M68K_Read_Word(cpu,cpu->address+2,(unsigned short *)&a);
					sprintf(cpu->istr,"%s%4.4X",cpu->istr,(unsigned short)a);
					break;

			}
		}
	}
}


//*******************************************************
//Decription:	Mode Register encode
//Argument:		Pointer CPU contex
//				Mode
//				Reg
//Return:		Data
//*******************************************************
bool OPStr_Mode_Encode(CPU_CONTEX *cpu,unsigned char mode,unsigned char reg, unsigned long data)
{
	bool retval = false;
	unsigned long t=0;

	if(cpu != NULL)
	{
		switch(mode)
		{
			case 0:	//Data Reg direct
				sprintf(cpu->istr,"%sD%d",cpu->istr,reg);
				break;

			case 1: //Address Reg direct
				sprintf(cpu->istr,"%sA%d",cpu->istr,reg);
				break;

			case 2: //Address Register Indirect
				t = M68K_Address_Reg_Read(cpu,reg);
				if(cpu->isize == 1) M68K_Write_Byte(cpu,t,data);
				if(cpu->isize == 3) M68K_Write_Word(cpu,t,data);
				if(cpu->isize == 2) M68K_Write_Long(cpu,t,data);
				break;

			case 7:	//Absolute Addressing
				if((reg == 0) || (reg == 1))
				{
					sprintf(cpu->istr,"%s$",cpu->istr);
					OPStr_Mode_Seven(cpu,mode,reg);
				}
				if(reg == 4)
				{
					sprintf(cpu->istr,"%sSR",cpu->istr);
				}
				break;

		}
	}
	return(retval);
}


//*******************************************************
//Decription:	Mode Register encode
//Argument:		Pointer CPU contex
//				Mode
//				Reg
//Return:		Data
//*******************************************************
char OPStr_Size(unsigned char sz)
{
	char retval = ' ';

	switch(sz)
	{
		case 0: //B for byte
			retval = 'B';
			break;

		case 2: //L long
			retval = 'L';
			break;

		case 1: //W word
			retval = 'W';
			break;
	}
	return(retval);
}

void OPS_ABCD(void *ptr)
{

}

void OPS_ADD(void *ptr)
{

}

void OPS_ORI(void *ptr)
{
	CPU_CONTEX *cpu = NULL;
	unsigned char dr;
	unsigned char dm;


	if(ptr != NULL)
	{
		cpu = ptr;

		sprintf(cpu->istr,"%8.8X %4.4X", (unsigned int)cpu->address,cpu->code);

		dr = cpu->code & 0x07;
		dm = (cpu->code >> 3) & 0x07;
		cpu->isize = (cpu->code >> 6) & 0x03;

		OPStr_Mode_Seven(cpu,dm,dr);
		sprintf(cpu->istr,"%s\tORI.%c\t",cpu->istr,OPStr_Size(0));
		sprintf(cpu->istr,"%s#$",cpu->istr);
		OPStr_Mode_Seven(cpu,dm,dr);
		sprintf(cpu->istr,"%s,",cpu->istr);
		OPStr_Mode_Encode(cpu,dm,dr,cpu->code);
	}
}

void OPS_BCLR(void *ptr)
{

}

void OPS_ANDI(void *ptr)
{

}

void OPS_SUBI(void *ptr)
{

}

void OPS_RTM(void *ptr)
{

}

void OPS_CALLM(void *ptr)
{

}

void OPS_ADDI(void *ptr)
{

}

void OPS_CMP(void *ptr)
{

}

void OPS_CHK(void *ptr)
{

}


void OPS_MOVE(void *ptr)
{
	CPU_CONTEX *cpu = NULL;
	MOVE_STRUCT *move = NULL;


	if(ptr != NULL)
	{
		cpu = (CPU_CONTEX *)ptr;

		sprintf(cpu->istr,"%8.8X %4.4X", (unsigned int)cpu->address,cpu->code); //diplay address and opcode
		move = (MOVE_STRUCT *)&cpu->code;

		OPStr_Mode_Seven(cpu,move->scr_mode,move->scr_mode);
		OPStr_Mode_Seven(cpu,move->dst_mode,move->dst_reg);

		sprintf(cpu->istr,"%s\tMOVE.%c\t",cpu->istr,OPStr_Size(move->size));
		OPStr_Mode_Encode(cpu,move->scr_mode,move->scr_reg,cpu->code);
		sprintf(cpu->istr,"%s,",cpu->istr);
		OPStr_Mode_Encode(cpu,move->dst_mode,move->dst_reg,cpu->code);
	}
}


/**
*
*
*/
void OPS_RESET(void *ptr)
{
	CPU_CONTEX *cpu = NULL;

	if(ptr != NULL)
	{
		cpu = (CPU_CONTEX *)ptr;


		sprintf(cpu->istr,"%8.8X %4.4X", (unsigned int)cpu->address,cpu->code);

		sprintf(cpu->istr,"%s\t\tRESET",cpu->istr);
	}
}

OPSTR_STRUCT m68k_opstr_table[] =
{
	{ 0xf1f0,0xc100, OPS_ABCD  },
	{ 0xf000,0xd000, OPS_ADD   },
	{ 0xff00,0x0000, OPS_ORI   },
	{ 0xffc0,0x0180, OPS_BCLR  },
	{ 0xffff,0x023c, OPS_ANDI  },
	{ 0xffff,0x027c, OPS_ANDI  },
	{ 0xff00,0x0200, OPS_ANDI  },
	{ 0xff00,0x0400, OPS_SUBI  },
	{ 0xfff0,0x06c0, OPS_RTM   },
	{ 0xffc0,0x06c0, OPS_CALLM },
	{ 0xff00,0x0600, OPS_ADDI  },
	{ 0xf9c0,0x09c0, OPS_CMP   },
	{ 0xf9c0,0x09c0, OPS_CHK   },
	{ 0xc000,0x0000, OPS_MOVE  },
	{ 0x4e70,0x4e70, OPS_RESET },
	{ 0xffff,0xffff, NULL     },
};


//************************************************************
//
//**************************************************************
void OPStr_Decode(CPU_CONTEX *cpu)
{
	OPSTR_STRUCT *ops = NULL;
	bool exit = false;
	int i;

	if(cpu != NULL)
	{
		if(cpu->istr != NULL) cpu->istr[0]=0; //make we have no string
		else exit = true;

		if(M68K_Read_Word(cpu,cpu->address,&cpu->code))
		{

			i = 0;
			while(!exit)
			{
				ops = &m68k_opstr_table[i];
				if(ops != NULL)
				{
					if((ops->opcode_handler == NULL) && (ops->mask == 0) && (ops->match == 0))
					{
						exit = true;
					}
					else
					{
						if((ops->mask & cpu->code) == ops->match)
						{
							(ops->opcode_handler)(cpu);
							exit = true;
						}
					}
				}
				else
				{
					exit = true;
				}
				i++;
			}
		}
	}
}

