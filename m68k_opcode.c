//**********************************************************
//Decode and Execute code
//*********************************************************

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>


#include "list.h"
#include "m68k_memory.h"
#include "m68kcpu.h"
#include "bit.h"
#include "m68k_opcode.h"


typedef union
{
	unsigned long ldata;
	int idata;
	struct _bytes
	{
		unsigned char cdata[4];
	}bytes;
	struct _shorts
	{
		unsigned short sdata[2];
	}shorts;
}DATA_TYPE_STRUCT;



/* This is used to generate the opcode handler jump table */
typedef struct
{
	unsigned int  mask;                  /* mask on opcode */
	unsigned int  match;                 /* what to match after masking */
	void (*opcode_handler)(void *);        /* handler function */
	//unsigned char cycles[NUM_CPU_TYPES]; /* cycles each cpu type takes */
}OPCODE_STRUCT;


//*******************************************************
//Decription:	Mode Register encode
//Argument:		Pointer CPU contex
//				Mode
//				Reg
//Return:		Data
//*******************************************************
unsigned long OPCode_Source_Mode_Encode(CPU_CONTEX *cpu,unsigned char mode,unsigned char reg)
{
	unsigned long retval = 0;

	if(cpu != NULL)
	{
		switch(mode)
		{
			case 0:	//Data Reg direct
				retval = cpu->d[reg];
				break;

			case 1: //Address Reg direct
				retval = M68K_Address_Reg_Read(cpu,reg);
				break;

			case 2: //Address Register Indirect
				retval = M68K_Address_Reg_Read(cpu,reg);
				M68K_Read_Long(cpu,retval,&retval);
				break;

			case 7:	//Absolute Addressing
				if(reg == 0)
				{
					M68K_Read_Word(cpu,cpu->pc,(unsigned short *)&retval);
					cpu->pc += 2;
				}
				else
				{
					M68K_Read_Long(cpu,cpu->pc,&retval);
					cpu->pc += 4;
				}
				break;

		}
	}
	return(retval);
}


//*******************************************************
//Decription:	Mode Register encode
//Argument:		Pointer CPU contex
//				Mode
//				Reg
//Return:		Data
//*******************************************************
bool OPCode_Destination_Mode_Encode(CPU_CONTEX *cpu,unsigned char mode,unsigned char reg, unsigned long data)
{
	bool retval = false;
	unsigned long t=0;

	if(cpu != NULL)
	{
		switch(mode)
		{
			case 0:	//Data Reg direct
				if(cpu->isize == 1) cpu->d[reg] = (cpu->d[reg] & 0xFFFFFF00) | (data & 0x000000FF);
				if(cpu->isize == 3) cpu->d[reg] = (cpu->d[reg] & 0xFFFF0000) | (data & 0x0000FFFF);
				if(cpu->isize == 2) cpu->d[reg] = data;
				break;

			case 1: //Address Reg direct
				t = M68K_Address_Reg_Read(cpu,reg);
				if(cpu->isize == 1) t = (t & 0xFFFFFF00) | (data & 0x000000FF);
				if(cpu->isize == 3) t = (t & 0xFFFF0000) | (data & 0x0000FFFF);
				if(cpu->isize == 2) t = data;
				M68K_Address_Reg_Write(cpu,reg,t);
				break;

			case 2: //Address Register Indirect
				t = M68K_Address_Reg_Read(cpu,reg);
				if(cpu->isize == 1) M68K_Write_Byte(cpu,t,data);
				if(cpu->isize == 3) M68K_Write_Word(cpu,t,data);
				if(cpu->isize == 2) M68K_Write_Long(cpu,t,data);
				break;

			case 7:	//Absolute Addressing
				if(reg == 0)
				{
					M68K_Read_Word(cpu,cpu->pc,(unsigned short *)&t);
					cpu->pc += 2;
				}
				else
				{
					M68K_Read_Long(cpu,cpu->pc,&t);
					cpu->pc += 4;
				}
				break;

		}
	}
	return(retval);
}


void OP_ABCD(void *ptr)
{

}

void OP_ADD(void *ptr)
{

}

void OP_ORI(void *ptr)
{
	CPU_CONTEX *cpu = NULL;
	unsigned char dr;
	unsigned char dm;
	unsigned short t;


	if(ptr != NULL)
	{
		cpu = (CPU_CONTEX *)ptr;

		//cpu->pc += 2;
		cpu->isize = (cpu->ir >> 6) & 0x03;
		dr = cpu->ir & 0x07; //three bit
		dm = (cpu->ir >> 3) & 0x07;

		switch(dm)
		{
			case 7:	//mode seven
				if(dr == 0)
				{

				}
				else if(dr == 1)
				{

				}
				else if(dr == 4)
				{
					if(M68K_Super_Read(cpu))
					{
						cpu->pc +=2;
						M68K_Read_Word(cpu,cpu->pc,&t);
						cpu->sr = cpu->sr | t;
						cpu->pc +=2;
					}
					else
					{
						cpu->status = CPU_STATUS_PRIVILEGE_VIOLATION;
					}
				}
		}
//		OPCode_Destination_Mode_Encode(cpu,dm,dr,OPCode_Source_Mode_Encode(cpu,sm,sr));
	}

}

void OP_BCLR(void *ptr)
{

}

void OP_ANDI(void *ptr)
{

}

void OP_SUBI(void *ptr)
{

}

void OP_RTM(void *ptr)
{

}

void OP_CALLM(void *ptr)
{

}

void OP_ADDI(void *ptr)
{

}

void OP_CMP(void *ptr)
{

}

void OP_CHK(void *ptr)
{

}


void OP_MOVE(void *ptr)
{
	CPU_CONTEX *cpu = NULL;
	MOVE_STRUCT *move = NULL;
	DATA_TYPE_STRUCT source;
	DATA_TYPE_STRUCT dest;



	if(ptr != NULL)
	{
		cpu = ptr;
		cpu->pc += 2;
		move = (MOVE_STRUCT *) &cpu->ir;
		switch(move->scr_mode)
		{
			case 0: //data register
				source.ldata = cpu->d[move->scr_reg];
				break;

			case 7: //special mode
				if(move->scr_reg == 1)	//absolute long
				{
					M68K_Read_Long(cpu,cpu->pc,&source.ldata);
					move->size = 3;
				}
				break;
		}


		switch(move->dst_mode)
		{
			case 0:	//Data registers direct
				switch(move->size)
				{
					case 3: // word
						cpu->d[move->dst_reg] = source.shorts.sdata[0];
						M68K_Neg_Write(cpu,(source.bytes.cdata[1] >> 7));
						M68K_Zero_Write(cpu,(source.shorts.sdata[0] == 0));
						M68K_VFlag_Write(cpu,false);
						M68K_Carry_Write(cpu,false);
						cpu->pc += 2;
						break;
				}
				break;

			case 1:	//Address register direct
				switch(move->size)
				{
					case 2: //long
						cpu->a[move->dst_reg] = source.ldata;
						M68K_Neg_Write(cpu,(source.bytes.cdata[3] >> 7));
						M68K_Zero_Write(cpu,(source.ldata == 0));
						M68K_VFlag_Write(cpu,false);
						M68K_Carry_Write(cpu,false);
						cpu->pc += 4;
						break;
				}
				break;


			case 7: //special mode
				if(move->dst_reg == 0)
				{
					dest.ldata =0;
					M68K_Read_Word(cpu,cpu->pc,&dest.shorts.sdata[0]);
					M68K_Neg_Write(cpu,bit_test_word(source.shorts.sdata[0],15));
                    M68K_Zero_Write(cpu,(source.bytes.cdata[0] == 0));
					M68K_VFlag_Write(cpu,false);
					M68K_Carry_Write(cpu,false);
					M68K_Write_Byte(cpu,dest.shorts.sdata[0],source.bytes.cdata[0]);
					cpu->pc += 2;
				}
				else // longs
				{
					M68K_Read_Long(cpu,cpu->pc,&dest.ldata);
					M68K_Neg_Write(cpu,(source.ldata >> 31));
                    M68K_Zero_Write(cpu,(source.ldata == 0));
					M68K_VFlag_Write(cpu,false);
					M68K_Carry_Write(cpu,false);
					M68K_Write_Byte(cpu,dest.ldata,source.ldata);
					cpu->pc += 4;
				}
				break;
		}
	}
}

/**
*
*
*/
void OP_RESET(void *ptr)
{
	CPU_CONTEX *cpu = NULL;

	if(ptr != NULL)
	{
		cpu = ptr;
		if(M68K_Super_Read(cpu))
        {
            M68K_Memory_Reset(cpu);	/*reset all memory modules*/
            cpu->pc += 2;
        }
        else
        {
            // do trap
            cpu->status = CPU_STATUS_PRIVILEGE_VIOLATION;
        }
	}
}

OPCODE_STRUCT m68k_opcode_table[] =
{
	{ 0xf1f0,0xc100, OP_ABCD  },
	{ 0xf000,0xd000, OP_ADD   },
	{ 0xff00,0x0000, OP_ORI   },
	{ 0xffc0,0x0180, OP_BCLR  },
	{ 0xffff,0x023c, OP_ANDI  },
	{ 0xffff,0x027c, OP_ANDI  },
	{ 0xff00,0x0200, OP_ANDI  },
	{ 0xff00,0x0400, OP_SUBI  },
	{ 0xfff0,0x06c0, OP_RTM   },
	{ 0xffc0,0x06c0, OP_CALLM },
	{ 0xff00,0x0600, OP_ADDI  },
	{ 0xf9c0,0x09c0, OP_CMP   },
	{ 0xf9c0,0x09c0, OP_CHK   },
	{ 0xc000,0x0000, OP_MOVE  },
	{ 0x4e70,0x4e70, OP_RESET },
	{ 0xffff,0xffff, NULL     },
};




//************************************************************
//
//**************************************************************
void OPCode_Decode(CPU_CONTEX *cpu)
{
	OPCODE_STRUCT *ops = NULL;
	bool exit = false;
	int i;

	i = 0;
	while(!exit)
	{

		ops = &m68k_opcode_table[i];
		if(ops != NULL)
		{
			if((ops->opcode_handler == NULL) && (ops->mask == 0) && (ops->match == 0))
			{
				exit = true;
			}
			else
			{
				if((ops->mask & cpu->ir) == ops->match)
				{
					(ops->opcode_handler)(cpu);
					exit = true;
				}
			}
		}
		i++;
	}
}
